@extends('layouts.master')
@section('titulo')
	Crear
@endsection
@section('contenido')
	<div class="row">
		<div class="offset-md-3 col-md-6">
			<div class="card">
				<div class="card-header text-center">
				 Añadir nuevo cuadro
				</div>
				<div class="card-body" style="padding:30px">
					<form action="{{ action('CuadrosController@postCrear') }}" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}

						<div class="form-group">
							<label for="nombre">Nombre del cuadro</label>
							<input type="text" name="nombre" id="nombre" class="form-control">
						</div>
						<div class="form-group">
							<label for="pintor">Pintor</label>
								<select name="pintor" id="pintor">
								@foreach($arrayPintores as $pintor)
										<option id="{{$pintor->id}}" name="{{$pintor->id}}" value="{{$pintor->id}}">{{$pintor->nombre}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label for="imagen">Imagen</label>
							<input type="file" name="imagen" id="imagen" class="form-control">
						</div>
						<div class="form-group text-center">
							<input type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;" value="Añadir cuadro">
						</div>
					</form>
				</div>
			</div>
		 </div>
	</div>
@endsection