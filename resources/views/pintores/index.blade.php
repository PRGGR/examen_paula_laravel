@extends('layouts.master')
@section('titulo')
	Index
@endsection
@section('contenido')
	<div class="row">

		<table class="table table-striped">
			<tr>
				<th>Nombre</th>
				<th>País</th>
				<th>Cuadros</th>
			</tr>
			@foreach($arrayPintores as $pintor)
			<tr>
				<td><a href="{{ url('/pintores/mostrar/' . $pintor->id ) }}">{{$pintor->nombre}}</a></td>
				<td>{{$pintor->pais}}</td>
				<td>{{$pintor->cuadros->count()}}</td>
			</tr>
			@endforeach
		</table>
	</div>
@endsection