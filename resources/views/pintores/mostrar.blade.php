@extends('layouts.master')
@section('titulo')
	Mostrar
@endsection
@section('contenido')
	<div class="row">		
		<div class="col-xs-12 col-sm-6">
				<h1>{{$pintor->nombre}}</h1>
				<h2>País: {{$pintor->pais}}</h2>
				<h3>Cuadros</h3>
					@php
						$cuadros = $pintor->cuadros;
					@endphp
					@foreach ($cuadros as $cuadro)
					<div class="col-xs-12 col-sm-6">
						<div style="margin-left: 50px">
							<p>{{$cuadro->nombre}}</p><br>
							<img src="{{asset('assets/imagenes/')}}/{{$cuadro->imagen}}" style="height:200px"/>
						</div>
					</div>
					@endforeach
			</div>
		</div>
	</div>
@endsection