<?php

namespace App\Http\Controllers;

use App\Cuadro;
use App\Pintor;
use Illuminate\Http\Request;

class CuadrosController extends Controller
{
    public function getCrear(){
    	$pintores = Pintor::all();
		return view('cuadros.crear', 
	    	array('arrayPintores' => $pintores));
	}

	public function postCrear(Request $request){
		
		$cuadro = new Cuadro();

		$cuadro->nombre = $request->nombre;
		$cuadro->imagen = $request->imagen->store('','cuadros');
		$cuadro->pintor_id = $request->pintor;

		try {
			$cuadro->save();
			return redirect('pintores')->with('mensaje', "Creado con éxito");
		} catch (Exception $ex){
			return redirect('pintores')->with('mensaje', "Fallo al crear el cuadro");
		}

	}
}
