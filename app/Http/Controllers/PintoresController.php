<?php

namespace App\Http\Controllers;

use App\Pintor;
use Illuminate\Http\Request;

class PintoresController extends Controller
{

	public function getInicio(){
    	return redirect()->action('PintoresController@getPintores');
    }

    public function getPintores(){
		
		$pintores = Pintor::all();
	    return view('pintores.index', 
	    	array('arrayPintores' => $pintores));
	}

	public function getMostrar($id_pintor){

		$pintor = Pintor::findOrFail($id_pintor);
	    return view('pintores.mostrar', 
	    	array('pintor' => $pintor));
	}
}
