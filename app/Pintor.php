<?php

namespace App;

use App\Cuadro;
use Illuminate\Database\Eloquent\Model;

class Pintor extends Model
{
    protected $table = 'pintores';

    public function cuadros(){
		return $this->hasMany('App\Cuadro');
	}

}
